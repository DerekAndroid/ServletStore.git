package cn.itcast.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AllServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    public String findAll(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("findAll");
        return "/all.jsp";
    }
}
